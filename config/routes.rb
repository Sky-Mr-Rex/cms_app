Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'home/index'
  devise_for :users
  resources :posts, only: [:index, :show]
  root to: 'posts#index'
end
