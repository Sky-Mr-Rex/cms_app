require 'rails_helper'

RSpec.describe Post, type: :model do
  before(:each) do
    @post = Post.new
  end

  it "valid with title" do
    @post.title = "Test Title"
    expect(@post).to be_valid
  end

  it "Not Valid without title" do
    expect(@post).to_not be_valid
  end

end
